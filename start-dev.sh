#!/bin/bash

# The purpose of this file is to assist in setting up a
# local development environment.


# Check to see if the script has been forked or not (we need it to not be)
[[ $0 != "$BASH_SOURCE" ]] && sourced=1 || sourced=0

if [[ $sourced == 0 ]]; then
  echo -e "Please run this script using 'source'. e.g. \n"
  echo -e "\tsource ./init.sh\n"
  exit 1
fi

# Check to see if we've created the virtualenv for the script
if [ -d "../env/mongoorm" ]; then
  echo -e "Virtual Env Exists. Continuing...\n"
else
  # Create the virtualenv using whatever python3 is installed globally
  virtualenv -p python3 ../env/mongoorm
fi

echo -e "\nRun 'deactivate' when finished\n"
source ../env/mongoorm/bin/activate
